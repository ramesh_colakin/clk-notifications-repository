package com.colakin.notifications.util;

import com.colakin.notifications.api.model.request.GraphQLQuery;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserQuery {
    public static UserQueryBuilder builder() {
        return new UserQueryBuilder();
    }

    public static class UserQueryBuilder extends GraphQLQueryBuilder {

        UserQueryBuilder() {
        }

        public GraphQLQuery build() {
            String queryStr = String.format("{User(filter:{keyCloakUserId: \"%s\" }, offset: 0,first: %s) " +
                    " {keyCloakUserId, deviceId, gender, contact{\n" +
                    "      primaryContactType,primaryContact,deviceId\n" +
                    "    }}}", getKeyCloakUserId(), getLimit());
            log.info("generated query for retrieving user ={}", queryStr);
            return GraphQLQuery
                    .builder()
                    .query(queryStr)
                    .build();
        }
    }
}
