package com.colakin.notifications.util;

import com.colakin.notifications.persistence.node.Measurement;
import lombok.Data;

import java.util.HashMap;

@Data
public abstract class GraphQLQueryBuilder {
    private String keyCloakUserId;
    private Measurement measurement;
    private int limit;

    public GraphQLQueryBuilder measurement(final Measurement measurement) {
        this.measurement = measurement;
        return this;
    }

    public GraphQLQueryBuilder limit(final int limit) {
        this.limit = limit;
        return this;
    }

    public GraphQLQueryBuilder keyCloakUserId(final String keyCloakUserId) {
        this.keyCloakUserId = keyCloakUserId;
        return this;
    }

    public abstract Object build();

    public String t3Query(Measurement measurement, HashMap<String, Double> deviationMap) {
        boolean measurementQueryAvailable = false;
        StringBuilder measurementQuery = new StringBuilder();
        //measurementQuery.append(",dressCode_in: { dressSize_some: {OR:[");
        if (measurement.getBottom().getHip() != null && !measurement.getTop().getMenOvalChest().equals(0d)) {
            buildMeasurement3("chest", measurementQuery, measurement, deviationMap);
            measurementQueryAvailable = true;
        }
        if (measurement.getBottom().getHip() != null && !measurement.getBottom().getHip().equals(0d)) {
            buildMeasurement3("hip", measurementQuery, measurement, deviationMap);
            measurementQueryAvailable = true;
        }
        if (measurement.getTop().getTopWaist() != null && !measurement.getTop().getTopWaist().equals(0d)) {
            buildMeasurement3("waist", measurementQuery, measurement, deviationMap);
            measurementQueryAvailable = true;
        }

        if (!measurementQueryAvailable) {
            //if no measurement available then
            return "";
        } else {
            //measurementQuery.append("]}}");
            return measurementQuery.toString();
        }
    }

    public void buildMeasurement(String parameter, StringBuilder measurementQuery, Measurement measurement) {
        measurementQuery.append("{");
        measurementQuery.append(parameter).append(": ");
       // measurementQuery.append(measurement.get(parameter));
        measurementQuery.append("}");
        measurementQuery.append(",");
    }

    public void buildMeasurement3(String parameter, StringBuilder measurementQuery, Measurement measurement, HashMap<String, Double> deviation) {
        measurementQuery.append("{AND:[");
        measurementQuery.append("{");
        measurementQuery.append(parameter).append("_gte: ");
        //measurementQuery.append(measurement.get(parameter) - deviation.get(parameter));
        measurementQuery.append("},");
        measurementQuery.append("{");
        measurementQuery.append(parameter).append("_lte: ");
        //measurementQuery.append(measurement.get(parameter) + deviation.get(parameter));
        measurementQuery.append("}");
        measurementQuery.append("]}");
        measurementQuery.append(",");
    }

    public String t2Query(Measurement measurement) {
        boolean measurementQueryAvailable = false;
        StringBuilder measurementQuery = new StringBuilder();
        measurementQuery.append(",dressCode_in: { dressSize_some: {OR:[");
        if (measurement.getTop().getMenOvalChest() != null && !measurement.getTop().getMenOvalChest().equals(0d)) {
            buildMeasurement("chest", measurementQuery, measurement);
            measurementQueryAvailable = true;
        }
        if (measurement.getBottom().getHip() != null && !measurement.getBottom().getHip().equals(0d)) {
            buildMeasurement("hip", measurementQuery, measurement);
            measurementQueryAvailable = true;
        }
        if (measurement.getTop().getTopWaist() != null && !measurement.getTop().getTopWaist().equals(0d)) {
            buildMeasurement("waist", measurementQuery, measurement);
            measurementQueryAvailable = true;
        }
        if (measurement.getTop().getAcrossShoulder() != null && !measurement.getTop().getAcrossShoulder().equals(0d)) {
            buildMeasurement("shoulder", measurementQuery, measurement);
            measurementQueryAvailable = true;
        }

        if (!measurementQueryAvailable) {
            //if no measurement available then
            return "";
        } else {
            measurementQuery.append("]}}");
            return measurementQuery.toString();
        }
    }

}
