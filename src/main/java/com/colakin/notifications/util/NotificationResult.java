package com.colakin.notifications.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationResult {
    @JsonProperty("message_id")
    private String messageId;

    @JsonProperty("registration_id")
    private String registrationId;

    @JsonProperty("error")
    private String error;
}
