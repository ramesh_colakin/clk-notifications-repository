package com.colakin.notifications.util;


public enum Priority {

    HIGH("high"), LOW("low");

    private String priority;

    Priority(String priority) {
        this.priority = priority;
    }

    public String getPriority() {
        return this.priority;
    }
}

