package com.colakin.notifications.util;


public enum TwilioIdentity {

    MOBILE("MOBILE"), WHATSAPP("WHATSAPP");

    private String identity;

    TwilioIdentity(String identity) {
        this.identity = identity;
    }

    public String getIdentity() {
        return this.identity;
    }
}

