package com.colakin.notifications.util;

public enum ContactType {
    mobile("mobile"),
    whatsapp("whatsapp"),
    email("email"),
    google("google"),
    facebook("facebook");
    private final String value;

    ContactType(String value) {
        this.value = value;
    }
}
