package com.colakin.notifications.service;

import com.colakin.notifications.api.model.response.ResponseMessage;
import com.colakin.notifications.util.Priority;
import reactor.core.publisher.Mono;

public interface FcmService {

    Mono<ResponseMessage> pushNotification(String fcmToken, String fcmServerKey, String appName, String pushMessage, Object data, String topics, Priority priorityLevel, String title);

}
