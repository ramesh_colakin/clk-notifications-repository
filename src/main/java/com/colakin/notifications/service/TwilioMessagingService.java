package com.colakin.notifications.service;

import com.colakin.notifications.api.model.request.TwilioMessageRequest;
import com.colakin.notifications.api.model.response.ResponseMessage;
import reactor.core.publisher.Mono;

public interface TwilioMessagingService {

    Mono<ResponseMessage> sendTwilioMessage(TwilioMessageRequest request,
                                            String appName);


}
