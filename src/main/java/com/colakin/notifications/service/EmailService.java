package com.colakin.notifications.service;

import com.colakin.notifications.api.model.response.ResponseMessage;
import reactor.core.publisher.Mono;

public interface EmailService {

    Mono<ResponseMessage> sendMail(String email, String emailFrom, String subject, String message);


}
