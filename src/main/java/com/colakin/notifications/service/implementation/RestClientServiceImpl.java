package com.colakin.notifications.service.implementation;

import com.colakin.notifications.api.model.request.PushNotificationRequest;
import com.colakin.notifications.api.model.request.SlackMessageRequest;
import com.colakin.notifications.api.model.response.PushNotificationResponse;
import com.colakin.notifications.common.Constants;
import com.colakin.notifications.config.RestClient;
import com.colakin.notifications.exceptions.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class RestClientServiceImpl {

    @Autowired
    private RestClient restClient;

    public Mono<PushNotificationResponse> notifyPost(String url, HttpHeaders httpHeaders, PushNotificationRequest pushNotificationRequest) {

        return restClient.post(url, httpHeaders, pushNotificationRequest, PushNotificationRequest.class)
                .flatMap(clientResponse -> {

                    HttpStatus status = clientResponse.statusCode();
                    log.info("HttpStatus : {}", status);
                    if (!status.is2xxSuccessful()) {
                        return Mono.error(new ApiException(Constants.FAILURE_PUSH, status, String.valueOf(status.value()), status.getReasonPhrase()));

                    }

                    log.info("Successfully posted notification userId={} ", pushNotificationRequest.getTo());
                    return clientResponse.bodyToMono(PushNotificationResponse.class);
                });
    }

    public Mono<String> notifySlack(String slackHook, HttpHeaders httpHeaders, SlackMessageRequest slackMessageRequest) {

        return restClient.post(slackHook, httpHeaders, slackMessageRequest, SlackMessageRequest.class)
                .flatMap(clientResponse -> {

                    HttpStatus status = clientResponse.statusCode();
                    log.info("HttpStatus : {}", status);
                    if (!status.is2xxSuccessful()) {
                        return Mono.error(new ApiException(Constants.FAILURE_SLACK, status, String.valueOf(status.value()), status.getReasonPhrase()));

                    }

                    return clientResponse.bodyToMono(String.class);
                });
    }


}
