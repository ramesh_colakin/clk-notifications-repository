package com.colakin.notifications.service.implementation;

import com.colakin.notifications.api.model.response.ResponseMessage;
import com.colakin.notifications.common.Constants;
import com.colakin.notifications.service.EmailService;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {

    @Autowired
    SendGrid sg;

    public Mono<ResponseMessage> sendMail(String email, String emailFrom, String subject, String message) {

        log.debug("Sending for user {}", email);
        Email from = new Email(emailFrom);
        Email to = new Email(email);
        Content content = new Content("text/html", message);
        Mail mail = new Mail(from, subject, to, content);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            log.info("Successfully sent to email:{} / {}", email, response.getStatusCode());
            return Mono.just(ResponseMessage.builder()
                    .to(email)
                    .message(Constants.SUCCESS_EMAIL)
                    .build());
        } catch (IOException ex) {
            log.error("Failed to send email:{} , with error ={}", email, ex.getMessage());
            return Mono.error(new com.colakin.notifications.exceptions.ApiException(Constants.FAILURE_EMAIL, HttpStatus.BAD_REQUEST, "400", ex.getMessage()));
        }
    }
}