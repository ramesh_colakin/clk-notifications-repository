package com.colakin.notifications.service.implementation;

import com.colakin.notifications.api.model.request.SlackMessageRequest;
import com.colakin.notifications.api.model.request.SlackRequest;
import com.colakin.notifications.api.model.response.ResponseMessage;
import com.colakin.notifications.service.SlackMessagingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;

import static com.colakin.notifications.common.Constants.FAILURE_SLACK;
import static com.colakin.notifications.common.Constants.SUCCESS_SLACK;

@Service
@Slf4j
public class SlackMessagingServiceImpl implements SlackMessagingService {

    @Autowired
    RestClientServiceImpl restClientService;

    public Mono<ResponseMessage> notifySlack(SlackRequest slackRequest, String slackHook, String appName) {

        return restClientService.notifySlack(slackHook, new HttpHeaders(), SlackMessageRequest.builder()
                .text(slackRequest.getMessage()).build())
                .flatMap(responseMessage -> {

                    return Mono.just(ResponseMessage.builder()
                            .to(slackRequest.getEventId())
                            .message((responseMessage.equals("ok")?SUCCESS_SLACK:FAILURE_SLACK)).build());
                });
    }
}