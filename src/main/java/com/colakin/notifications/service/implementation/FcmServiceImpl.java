package com.colakin.notifications.service.implementation;

import com.colakin.notifications.api.model.request.PushNotificationRequest;
import com.colakin.notifications.api.model.response.PushNotificationResponse;
import com.colakin.notifications.api.model.response.ResponseMessage;
import com.colakin.notifications.common.Constants;
import com.colakin.notifications.config.RestClient;
import com.colakin.notifications.service.FcmService;
import com.colakin.notifications.service.model.Message;
import com.colakin.notifications.service.model.Notification;
import com.colakin.notifications.util.Priority;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class FcmServiceImpl implements FcmService {

    @Autowired
    RestClient restClient;

    @Autowired
    RestClientServiceImpl restClientService;

    @Value("${fcm.api.url}")
    private String fcmURL;

    private final String PRIORITY_DEFAULT = "HIGH";

    @Override
    public Mono<ResponseMessage> pushNotification(String target,
                                                  String fcmServerKey,
                                                  String appName,
                                                  String pushMessage,
                                                  Object data,
                                                  String topics, Priority level, String title) {

        //0. validate the serverKey, message, target is not empty
        //1. setting up headers
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.AUTHORIZATION, "key=" + fcmServerKey);

        //2. construct request body
        Message msg = Message.builder()
                .message(pushMessage)
                .build();

        Notification notification = Notification.builder()
                .body(pushMessage)
                .title(title)
                .build();


        //Iterator<String> iterator = target.iterator();
        ResponseEntity<PushNotificationResponse> responseEntity;
        List<ResponseMessage> results = new ArrayList<>();

        PushNotificationRequest request = PushNotificationRequest.builder()
                .priority(PRIORITY_DEFAULT)
                .notification(notification)
                .data(data)
                .to(target)
                .build();

        //HttpEntity<PushNotificationRequest> requestEntity = new HttpEntity<>(request, headers);
        //3. invoke api

        return notifyPost(fcmURL + "/send", httpHeaders, request).flatMap(res -> {
            return Mono.just(ResponseMessage.builder()
                    .to(request.getTo())
                    .message((res.getSuccess() == 1) ? Constants.SUCCESS_PUSH : Constants.FAILURE_PUSH)
                    .build());
        });


    }


    private Mono<PushNotificationResponse> notifyPost(String url, HttpHeaders httpHeaders, PushNotificationRequest pushNotificationRequest) {

        return restClientService.notifyPost(url, httpHeaders, pushNotificationRequest)
                .flatMap(responseMessage -> {

                    return Mono.just(responseMessage);
                });
    }


}
