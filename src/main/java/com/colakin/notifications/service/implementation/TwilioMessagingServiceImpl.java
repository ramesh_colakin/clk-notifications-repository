package com.colakin.notifications.service.implementation;

import com.colakin.notifications.api.model.request.TwilioMessageRequest;
import com.colakin.notifications.api.model.response.ResponseMessage;
import com.colakin.notifications.common.Constants;
import com.colakin.notifications.service.TwilioMessagingService;
import com.colakin.notifications.util.TwilioIdentity;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import static com.colakin.notifications.common.Constants.*;

@Service
@Slf4j
public class TwilioMessagingServiceImpl implements TwilioMessagingService {

    @Autowired
    RestClientServiceImpl restClientService;

    public Mono<ResponseMessage> sendTwilioMessage(TwilioMessageRequest request,
                                                   String appName) {

        log.debug("Sending message {}", request.getSmsFormat());
        Twilio.init(TWILIO_SID, TWILIO_TOKEN);
        try {

            switch (request.getUserIdentity()) {

                case MOBILE:
                    Message message = Message.creator(
                            new com.twilio.type.PhoneNumber(request.getUserID()),
                            new com.twilio.type.PhoneNumber(TWILIO_NUMBER),
                            request.getSmsFormat())
                            .create();
                    log.debug("Received response from twilio for {} ={}", request.getUserID(), message.toString());
                    return Mono.just(ResponseMessage.builder()
                            .to(request.getUserID())
                            .message(Constants.SUCCESS_SMS)
                            .build());

                case WHATSAPP:
                    Message message1 = Message.creator(
                            new com.twilio.type.PhoneNumber(String.format("whatsapp:%s", request.getUserID())),
                            new com.twilio.type.PhoneNumber(String.format("whatsapp:%s", TWILIO_WHATSAPP_NUMBER)),
                            request.getSmsFormat())
                            .create();
                    log.debug("Received response from twilio for {} ={}", request.getUserID(), message1.toString());
                    return Mono.just(ResponseMessage.builder()
                            .to(request.getUserID())
                            .message(Constants.SUCCESS_SMS)
                            .build());

                default:
                    return Mono.error(new com.colakin.notifications.exceptions.ApiException("Invalid identity, accept only Mobile / WhatsApp", HttpStatus.BAD_REQUEST, "400", "Invalid identity"));

            }


        } catch (ApiException ex) {
            log.error("Failed to send otp, received error response from twilio for {} ={}", request.getUserID(), ex.getMessage());
            return Mono.error(new com.colakin.notifications.exceptions.ApiException(Constants.FAILURE_SMS, HttpStatus.BAD_REQUEST, "400", ex.getMessage()));
        }
    }
}