package com.colakin.notifications.service;

import com.colakin.notifications.api.model.request.SlackRequest;
import com.colakin.notifications.api.model.response.ResponseMessage;
import reactor.core.publisher.Mono;

public interface SlackMessagingService {

    Mono<ResponseMessage> notifySlack(SlackRequest request, String slackHook, String appName);


}
