package com.colakin.notifications.common;

public class Constants {

    public static final String API_BASE_CONTEXT_PATH = "/notification";
    public static final String API_SEND_CONTEXT_PATH = "/send";
    public static final String API_PUSH_CONTEXT_PATH = "/push";
    public static final String API_SLACK_CONTEXT_PATH = "/slack";
    public static final String API_TWILIO_CONTEXT_PATH = "/sms";
    public static final String API_EMAIL_CONTEXT_PATH = "/email";
    public static final String API_VERSION_CONTEXT_PATH = API_BASE_CONTEXT_PATH + "/version";

    public static final String SUCCESS_PUSH = "Push notification send successfully";
    public static final String FAILURE_PUSH = "Invalid push request";
    public static final String SUCCESS_SLACK = "Slack notification send successfully";
    public static final String FAILURE_SLACK = "Invalid slack request";

    public static final String SUCCESS_SMS = "Send message successfully";
    public static final String FAILURE_SMS = "Send message failed";
    public static final String SUCCESS_EMAIL = "Send e-mail successfully";
    public static final String FAILURE_EMAIL = "Send e-mail failed";

    public static final String TWILIO_NUMBER = "+12055089721";
    public static final String TWILIO_WHATSAPP_NUMBER = "+14155238886";
    public static final String TWILIO_SID = "AC769451033f066faa0268128216bc3262";
    public static final String TWILIO_TOKEN = "046c2d3f4d3a46fe9afd8c4047c11fec";

    public static class Identity {
        public static final String MOBILE = "Mobile";
        public static final String WHATSAPP = "WhatsApp";
        public static final String EMAIL = "Email";
    }

    public class Header {
        public static final String USER_ID = "UserId";
    }

}
