package com.colakin.notifications;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
@EnableWebFlux
@Slf4j
public class ClkNotificationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClkNotificationsApplication.class, args);
        log.info("Starting Colakins notifications services...");
    }


}
