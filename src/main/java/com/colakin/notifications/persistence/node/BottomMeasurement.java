package com.colakin.notifications.persistence.node;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BottomMeasurement {
    @JsonProperty("bottomWaist")
    private Double bottomWaist;
    @JsonProperty("hip")
    private Double hip;
    @JsonProperty("inSeamLength")
    private Double inSeamLength;
    @JsonProperty("outSeamLength")
    private Double outSeamLength;
}
