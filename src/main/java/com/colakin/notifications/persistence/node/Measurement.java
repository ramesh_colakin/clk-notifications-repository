package com.colakin.notifications.persistence.node;

import lombok.*;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Measurement {

    private TopMeasurement top;
    private BottomMeasurement bottom;

    // actual height
    private Double height;
}
