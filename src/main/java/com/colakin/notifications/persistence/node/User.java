package com.colakin.notifications.persistence.node;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @JsonProperty("keyCloakUserId")
    private String keyCloakUserId;
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("country")
    private String country;
    @JsonProperty("status")
    private String status;
    @JsonProperty("contact")
    private Contact contact;
}
