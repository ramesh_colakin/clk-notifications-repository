package com.colakin.notifications.persistence.node;

import com.colakin.notifications.util.ContactType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;



@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Contact {
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("primaryContact")
    private String primaryContact;
    @JsonProperty("primaryContactType")
    private ContactType primaryContactType;
}
