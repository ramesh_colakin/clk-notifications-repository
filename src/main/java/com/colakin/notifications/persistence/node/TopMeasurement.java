package com.colakin.notifications.persistence.node;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TopMeasurement {

    @JsonProperty("acrossShoulder")
    private Double acrossShoulder;
    @JsonProperty("menOvalChest")
    private Double menOvalChest;
    @JsonProperty("topFrontLength")
    private Double topFrontLength;
    @JsonProperty("womenOvalBust")
    private Double womenOvalBust;
    @JsonProperty("topWaist")
    private Double topWaist;
}
