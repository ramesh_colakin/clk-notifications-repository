package com.colakin.notifications.api;

import com.colakin.notifications.api.model.Project;
import com.colakin.notifications.common.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = {Constants.API_BASE_CONTEXT_PATH}, produces = {MediaType.APPLICATION_JSON_VALUE})
@Slf4j
public class ApplicationApi {

    @Value("${project.name}")
    private String name;
    @Value("${project.version}")
    private String version;

    @GetMapping(path = {"/version"})
    public Mono<ResponseEntity<Project>> getVersion() {
        Project project = Project.builder().name(name).version(version).build();
        return Mono.just(ResponseEntity.ok().body(project));
    }
}