package com.colakin.notifications.api.model.response;

import com.colakin.notifications.util.NotificationResult;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PushNotificationResponse {
    private long multicast_id;

    private int success;

    private int failure;

    private int canonical_ids;

    private List<NotificationResult> results = new ArrayList<>();

    /**
     * Error response attributes
     **/
    @JsonProperty("message_id")
    private String messageId;

    @JsonProperty("error")
    private String error;
}
