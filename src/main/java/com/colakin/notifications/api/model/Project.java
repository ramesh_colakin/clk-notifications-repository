package com.colakin.notifications.api.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Project {

    private String name;
    private String version;

}
