package com.colakin.notifications.api.model.request;

import com.colakin.notifications.util.Priority;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PushListRequest {

    private String title;
    private String fcmServerKey;
    private List<String> to;
    private String message;
    private Object notificationContent;
    private String topics;
    private Priority priority;

}
