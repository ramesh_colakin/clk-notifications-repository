package com.colakin.notifications.api.model.request;

import com.colakin.notifications.util.TwilioIdentity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TwilioMessageRequest {
    @NotBlank(message = "UserID is mandatory (Mobile number|WhatsApp number)")
    private String userID;
    @NotBlank(message = "UserIdentity is mandatory (MOBILE|WHATSAPP)")
    private TwilioIdentity userIdentity;
    @NotBlank(message = "SMS Format is mandatory")
    private String smsFormat;
}
