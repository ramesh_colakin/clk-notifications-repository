package com.colakin.notifications.api.model.request;

import com.colakin.notifications.util.Priority;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PushRequest {

    private String title;
    //private String fcmServerKey;
    private String to;
    private String message;
    private Object notificationContent;
    private String topics;
    private Priority priority;

}
