package com.colakin.notifications.api.model.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GraphQLQuery {
    private String query;
}
