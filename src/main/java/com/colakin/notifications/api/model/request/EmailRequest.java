package com.colakin.notifications.api.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmailRequest {
    @NotBlank(message = "To email is mandatory")
    private String email;
    @NotBlank(message = "Subject is mandatory")
    private String subject;
    @NotBlank(message = "Message is mandatory")
    private String message;
}
