package com.colakin.notifications.api.model;

import com.colakin.notifications.persistence.node.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by mani on 16/07/2020.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GraphQLUserData {
    @JsonProperty("User")
    private List<User> User;
}
