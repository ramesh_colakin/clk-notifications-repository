package com.colakin.notifications.api.model.response;

import com.colakin.notifications.api.model.GraphQLUserData;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mani on 16/07/2020.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GraphQLUserResponse {
    @JsonProperty("data")
    private GraphQLUserData data;
}
