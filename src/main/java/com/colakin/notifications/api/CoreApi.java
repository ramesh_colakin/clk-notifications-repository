package com.colakin.notifications.api;


import com.colakin.notifications.api.model.request.*;
import com.colakin.notifications.api.model.response.ResponseMessage;
import com.colakin.notifications.common.Constants;
import com.colakin.notifications.service.EmailService;
import com.colakin.notifications.service.FcmService;
import com.colakin.notifications.service.TwilioMessagingService;
import com.colakin.notifications.service.implementation.SlackMessagingServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping(path = {Constants.API_BASE_CONTEXT_PATH}, produces = {MediaType.APPLICATION_JSON_VALUE})
@Slf4j
public class CoreApi {

    @Autowired
    private FcmService fcmService;

    @Autowired
    SlackMessagingServiceImpl slackMessagingService;

    @Autowired
    TwilioMessagingService twilioMessagingService;

    @Autowired
    EmailService emailService;

    //REST mapping
    @PostMapping(Constants.API_PUSH_CONTEXT_PATH)
    public Mono<ResponseMessage> notifyPush(@RequestBody PushRequest request,
                                            @RequestHeader("serverKey") String fcmServerKey,
                                            @RequestHeader("sourceSystem") String appName) {

        log.info("Request received from app: {}  to send push notifications to request={}", appName, request.toString());
        return fcmService.pushNotification(request.getTo(),
                fcmServerKey,
                appName,
                request.getMessage(),
                request.getNotificationContent(),
                request.getTopics(),
                request.getPriority(),
                request.getTitle());
    }

    @PostMapping(Constants.API_SLACK_CONTEXT_PATH)
    public Mono<ResponseMessage> notifySlack(@RequestBody SlackRequest request,
                                             @RequestHeader("slackUrl") String slackHook,
                                             @RequestHeader("sourceSystem") String appName) {

        log.info("Request received from app: {} to send slack notifications to request={}", appName, request.toString());
        return slackMessagingService.notifySlack(request, slackHook, appName);
    }

    @PostMapping(Constants.API_TWILIO_CONTEXT_PATH)
    public Mono<ResponseMessage> sendTwilioMessage(@RequestBody TwilioMessageRequest request,
                                                   @RequestHeader("sourceSystem") String appName) {

        log.info("Request received from app: {} to send sms message to request={}", appName, request.toString());
        return twilioMessagingService.sendTwilioMessage(request, appName);
    }

    @PostMapping(Constants.API_EMAIL_CONTEXT_PATH)
    public Mono<ResponseMessage> sendMail(@RequestBody EmailRequest request,
                                          @RequestHeader("emailFrom") String emailFrom,
                                          @RequestHeader("sourceSystem") String appName) {

        log.info("Request received from app: {} to send sms message to request={}", appName, request.toString());
        return emailService.sendMail(request.getEmail(), emailFrom, request.getSubject(), request.getMessage());
    }
}
