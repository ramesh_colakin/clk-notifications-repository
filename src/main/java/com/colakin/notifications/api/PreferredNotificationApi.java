package com.colakin.notifications.api;

import com.colakin.notifications.api.model.request.GraphQLQuery;
import com.colakin.notifications.api.model.request.NotificationRequest;
import com.colakin.notifications.api.model.request.TwilioMessageRequest;
import com.colakin.notifications.api.model.response.GraphQLUserResponse;
import com.colakin.notifications.common.Constants;
import com.colakin.notifications.exceptions.ApiException;
import com.colakin.notifications.persistence.node.Contact;
import com.colakin.notifications.persistence.node.User;
import com.colakin.notifications.service.EmailService;
import com.colakin.notifications.service.FcmService;
import com.colakin.notifications.service.TwilioMessagingService;
import com.colakin.notifications.service.implementation.SlackMessagingServiceImpl;
import com.colakin.notifications.util.ContactType;
import com.colakin.notifications.util.UserQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static com.colakin.notifications.util.TwilioIdentity.MOBILE;
import static com.colakin.notifications.util.TwilioIdentity.WHATSAPP;

@RestController
@RequestMapping(path = {Constants.API_BASE_CONTEXT_PATH}, produces = {MediaType.APPLICATION_JSON_VALUE})
@Slf4j
public class PreferredNotificationApi {

    @Autowired
    private FcmService fcmService;

    @Autowired
    SlackMessagingServiceImpl slackMessagingService;

    @Autowired
    TwilioMessagingService twilioMessagingService;

    @Autowired
    EmailService emailService;

    @Value("${config.graphql.url}")
    String graphqlUrl;

    @PostMapping(Constants.API_SEND_CONTEXT_PATH)
    public Mono<?> sendNotification(@RequestBody NotificationRequest request,
                                    @RequestHeader("sourceSystem") String appName) throws ApiException {
        GraphQLQuery userContactQuery = (GraphQLQuery) UserQuery.builder()
                .keyCloakUserId(request.getUserId())
                .limit(1)
                .build();
        try {
            return callGraphQL(userContactQuery)
                    .flatMap(result -> {
                        //GraphQLMeasurementResponse res=OBJECT_MAPPER.convertValue(result, GraphQLMeasurementResponse.class);
                        if (result.getData().getUser().size() > 0) {
                            return processNotification(result.getData().getUser().get(0), request, appName);
                        } else {
                            return Mono.error(new ApiException("We can't send notification to the user.", HttpStatus.NOT_FOUND, "404", "User not found."));
                        }

                    })
                    .switchIfEmpty(Mono.error(new ApiException("We can't send notification to the user", HttpStatus.NOT_FOUND, "404", "User not found")));
        } catch (Exception e) {
            throw new ApiException("We can't send notification to the user.", HttpStatus.BAD_REQUEST, "400", e.getLocalizedMessage());
        }
    }

    private Mono<?> processNotification(User user, NotificationRequest request, String appName) {

//        user.setContact(Contact.builder()
//                .primaryContact("developermanig@gmail.com")
//                .primaryContactType(ContactType.email)
//                .build());
        if (user.getContact() != null) {
            switch (user.getContact().getPrimaryContactType().toString()) {
                case "email":
                    log.info("Request received from app: {} to send mail message to request={}", appName, user.getContact().toString());
                    return emailService.sendMail(user.getContact().getPrimaryContact(),
                            request.getEmailFrom(),
                            request.getMessageTitle(),
                            request.getMessage());
                case "mobile":
                    log.info("Request received from app: {} to send sms message to request={}", appName, request.toString());
                    return twilioMessagingService.sendTwilioMessage(TwilioMessageRequest.builder()
                                    .smsFormat(request.getMessageTitle() + "\n" + request.getMessage())
                                    .userID(user.getContact().getPrimaryContact())
                                    .userIdentity(MOBILE)
                                    .build(),
                            appName);
                case "whatsapp":
                    log.info("Request received from app: {} to send sms message to request={}", appName, request.toString());
                    return twilioMessagingService.sendTwilioMessage(TwilioMessageRequest.builder()
                                    .smsFormat(request.getMessageTitle() + "\n" + request.getMessage())
                                    .userID(user.getContact().getPrimaryContact())
                                    .userIdentity(WHATSAPP)
                                    .build(),
                            appName);
            }
        }
        return Mono.error(new ApiException("We can't send notification to the user.", HttpStatus.BAD_REQUEST, "400", "We can't send notification to user."));
    }

    private Mono<GraphQLUserResponse> callGraphQL(GraphQLQuery query) throws ApiException {
        log.debug("graphql query generated ={}", query);
        try {
            return WebClient.create()
                    .post()
                    .uri(graphqlUrl)
                    .body(BodyInserters.fromValue(query))
                    .retrieve()
                    .bodyToMono(GraphQLUserResponse.class);
        } catch (Exception e) {
            throw new ApiException("We can't send notification to the user.", HttpStatus.BAD_REQUEST, "400", e.getLocalizedMessage());
        }
    }
}
